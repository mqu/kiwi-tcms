# Kiwi-TCMS 

the goal of this project is to provide a docker registry for Kiwi-TCMS application with tagged versions on a public Docker Registry. We are going to publish image on Gitlab registry.

Usage :

```
# choose one version : 11.4 or latest ; will have more TAGs in the future
# version=latest
version=11.4
docker run --rm -it registry.gitlab.com/mqu/kiwi-tcms:${version} bash

# display version : (in the Kiwi container)
bash -c "cd /Kiwi ; python -m tcms" 
```

# Registry

Registry container is visible at : https://gitlab.com/mqu/kiwi-tcms/container_registry

# Tags update

Tag will update monthly, when there will be a new release published on docker-hub. Periodic task append on [first on month, at 2h00](https://gitlab.com/mqu/kiwi-tcms/-/pipeline_schedules).

## links:
- https://github.com/kiwitcms/Kiwi
- https://kiwitcms.org/
- https://kiwitcms.readthedocs.io/en/latest/
- https://github.com/kiwitcms/Kiwi/issues/2596

## How

Howto pull latest version and push it to public registries. This task may be done periodicaly by some tasks (gitlab-ci, github-ci).

```
# download latest version ; don't know with one :-(
docker pull kiwitcms/kiwi:latest

# get version now
version=$(docker run --rm -it kiwitcms/kiwi:latest bash -c "cd /Kiwi ; python -m tcms")

# now we can TAG this Docker container
docker tag kiwitcms/kiwi:latest kiwitcms/kiwi:${version}

# want to push to public registry ?
# registry.gitlab.com
docker tag kiwitcms/kiwi:latest registry.gitlab.com/mqu/kiwi-tcms:${version}
docker tag kiwitcms/kiwi:latest registry.gitlab.com/mqu/kiwi-tcms:latest

# now push to registry ; may need to login first
docker push registry.gitlab.com/mqu/kiwi-tcms:${version}
docker push registry.gitlab.com/mqu/kiwi-tcms:latest
```

